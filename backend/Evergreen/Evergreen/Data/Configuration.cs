﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Evergreen.Data
{
    public static class Configuration
    {
        public static string ConnectionString = @"Host=localhost;Database=evergreen;Username=postgres;Password=mysecretpassword";
    }
}
