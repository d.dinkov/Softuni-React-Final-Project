﻿using Evergreen.Model;
using Evergreen.Services.Conracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Evergreen.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            this._productService = productService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProductForListViewModel>> GetForList(int categoryId)
        {
            var products = await this._productService.GetForList(categoryId);

            return products;
        }

        [HttpGet("getById")]
        public async Task<ProductByIdViewModel> GetById(int id)
        {
            return await this._productService.GetById(id);
        }

        [HttpGet("search")]
        public async Task<IEnumerable<ProductForListViewModel>> Search(string keyword)
        {
            return await this._productService.Search(keyword);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateProductInputModel input)
        {
            await this._productService.Add(input);
            return this.Ok();
        }
    }
}
