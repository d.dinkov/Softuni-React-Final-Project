﻿using Evergreen.Model;
using Evergreen.Services.Conracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Evergreen.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            this._contactService = contactService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ContactMessageInputModel message)
        {
            await this._contactService.MakeMessage(message);
            return this.Ok();
        }

        [HttpGet]
        public async Task<IEnumerable<MessageViewModel>> Get()
        {
            return await this._contactService.GetMessages();
        }
    }
}
